This is a fork of rosdistro created to add custom rosdep rules to the default set.
Unfortunately thanks to the LRZ gitlab settings, raw files cannot be downloaded
without authentication (i.e. rosdep cannot use them). So an additional github mirror
will be needed. At the time being, it is github.com/marcoesposito1988/rosdistro.git

The current branch was created to replace the default README with what you are reading:
DO NOT MERGE THIS CHANGE WITH UPSTREAM if you want
to contribute rules back. Do all changes in another branch and merge it with this one.

rosdep can be used to install automatically all dependencies of a set of ROS packages.
The fastest way is:
```rosdep install --from-paths PATH_TO_YOUR_WORKSPACE_SRC_DIR --ignore-src -yir```

But we at IFL use some packages that are not available through the default rules. So we need some more.
It would be nice to contribute them to upstream one day.

In order to use the rules contained here:
- add a file to /etc/ros/rosdep/sources.list.d with the name ```10-IFL.list``` and the
following content: 
```
yaml https://raw.githubusercontent.com/marcoesposito1988/rosdistro/master/rosdep/base.yaml 
yaml https://raw.githubusercontent.com/marcoesposito1988/rosdistro/master/rosdep/python.yaml```

- ```rosdep update```
- check that the rules work, for example: ```rosdep resolve openigtlink```

How to add rules:
- Add a rule to rosdep/base.yaml or rosdep/python.yaml
- push to this gitlab mirror
- push to the github mirror needed to download raw content (currently marcoesposito1988/rosdistro)
- ```rosdep update```
